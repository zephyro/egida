<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="main_header main_header_gradient">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li>Новости</li>
						</ul>
					</div>
				</div>
				<div class="header_content">
					<div class="container">
						<h1>Новости</h1>
					</div>
				</div>
			</div>

		    <section class="main">
			    <div class="container container_long">
					<div class="main__row">

						<div class="main__sidebar">
							<nav class="sidenav sticky">
								<ul class="sidenav_first">
									<li>
										<a href="#"><span><i>ПРЕСС-ЦЕНТР</i></span></a>
									</li>
									<li>
										<a href="#"><span><i>СОБЫТИЯ ОТРАСЛИ</i></span></a>
									</li>
								</ul>
							</nav>
						</div>

						<div class="main__content">

							<article class="news_box">
								<a class="news_box__image" href="">
									<div class="news_box__image_wrap">
										<img src="img/news-item.jpg" class="img-fluid" alt="">
									</div>
								</a>
								<div class="news_box__content">
									<div class="news_box__title">
										<a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a>
									</div>
									<div class="news_box__date">14.02.2018</div>
									<div class="news_box__tag">
										<a href="#">Новости компании</a>
									</div>
									<div class="news_box__intro">
										17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного полигона в пгт. Ахтырский, руководством
										ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
										№ 52 общим объемом 13 тонн.
									</div>
									<div class="news_box__link">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</article>

							<article class="news_box">
								<a class="news_box__image" href="">
									<div class="news_box__image_wrap">
										<img src="img/news-item.jpg" class="img-fluid" alt="">
									</div>
								</a>
								<div class="news_box__content">
									<div class="news_box__title">
										<a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a>
									</div>
									<div class="news_box__date">14.02.2018</div>
									<div class="news_box__tag">
										<a href="#">Новости компании</a>
									</div>
									<div class="news_box__intro">
										17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного полигона в пгт. Ахтырский, руководством
										ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
										№ 52 общим объемом 13 тонн.
									</div>
									<div class="news_box__link">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</article>

							<article class="news_box">
								<a class="news_box__image" href="">
									<div class="news_box__image_wrap">
										<img src="img/news-item.jpg" class="img-fluid" alt="">
									</div>
								</a>
								<div class="news_box__content">
									<div class="news_box__title">
										<a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a>
									</div>
									<div class="news_box__date">14.02.2018</div>
									<div class="news_box__tag">
										<a href="#">Новости компании</a>
									</div>
									<div class="news_box__intro">
										17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного полигона в пгт. Ахтырский, руководством
										ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
										№ 52 общим объемом 13 тонн.
									</div>
									<div class="news_box__link">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</article>

							<ul class="pagination">
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&rarr;</a></li>
							</ul>

						</div>

					</div>
			    </div>
		    </section>

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Modal Order -->
		<div class="hide">
			<div class="modal" id="request">
				<div class="modal__title color-orange">Отправить запрос</div>
				<div class="modal__product">Пенообразователь «Транспортный» НСВ</div>
				<form class="form">

					<div class="form_row">
						<div class="form_col_55">
							<input type="text" class="form-control" name="" placeholder="Объём пенообразователя*">
						</div>
						<div class="form_col_45">
							<select class="form-select">
								<option value="">ед. изм.</option>
								<option value="мм.">мм.</option>
								<option value="см.">см.</option>
								<option value="м.">м.</option>
								<option value="км.">км.</option>
							</select>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Концентрация пенообразователя">
								<span class="form_legend_text">%</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Температура замерзания">
								<span class="form_legend_text">C</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваше имя*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Организация">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваш телефон">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="E-mail*">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-orange btn-send">Отправить</button>
					</div>
				</form>
			</div>
		</div>
		<!-- -->

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
