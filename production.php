<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

		    <section class="productions">
		        <div class="breadcrumbs">
		            <div class="breadcrumbs__container">
		                <ul class="breadcrumb">
		                    <li><a href="#">Главная</a></li>
		                    <li>Продукция</li>
		                </ul>
		            </div>
		        </div>
		        <div class="container">

		            <h1>Продукция</h1>

		            <div class="product-box">
		                <div class="product-box__layout"></div>
		                <ul class="product-box__list">
		                    <li>
		                        <a href="#">
		                            <div class="product-box__icon">
		                                <img src="img/production__01.png" class="img-fluid" alt="">
		                            </div>
		                            <div class="product-box__text">
		                                        <span>
		                                            ПЕНООБРАЗОВАТЕЛИ<br/>
		                                            ДЛЯ ПОЖАРОТУШЕНИЯ
		                                        </span>
		                            </div>
		                        </a>
		                    </li>
		                    <li>
		                        <a href="#">
		                            <div class="product-box__icon">
		                                <img src="img/production__02.png" class="img-fluid" alt="">
		                            </div>
		                            <div class="product-box__text">
		                                <span>СМАЧИВАТЕЛИ</span>
		                            </div>
		                        </a>
		                    </li>
		                    <li>
		                        <a href="#">
		                            <div class="product-box__icon">
		                                <img src="img/production__03.png" class="img-fluid" alt="">
		                            </div>
		                            <div class="product-box__text">
		                                        <span>
		                                            ПЕНООБРАЗОВАТЕЛИ<br/>
		                                            ДЛЯ ПЕНОБЕТОНА
		                                        </span>
		                            </div>
		                        </a>
		                    </li>
		                    <li>
		                        <a href="#">
		                            <div class="product-box__icon">
		                                <img src="img/production__04.png" class="img-fluid" alt="">
		                            </div>
		                            <div class="product-box__text">
		                                        <span>
		                                            ПОЖАРНО-ТЕХНИЧЕСКОЕ<br/>
		                                            ОБОРУДОВАНИЕ
		                                        </span>
		                            </div>
		                        </a>
		                    </li>
		                </ul>
		            </div>

		            <div class="productions__text">
		                Одним из приоритетных направлений в пожаротушении являются пенные  системы пожаротушения. Это обусловлено тем, что до сегодняшнего дня наиболее эффективным и, пожалуй, единственным, способом борьбы с пожарами нефти и нефтепродуктов является пожарная пена.
		            </div>

		        </div>
		    </section>

		    <section class="contact-info">
		        <div class="container">
		            <div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
		            <ul class="contact-info__phone">
		                <li>
		                    <a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
		                </li>
		                <li>
		                    <a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
		                </li>
		            </ul>
		            <div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
		            <div class="text-center">
		                <a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
		            </div>
		        </div>
		    </section>

		    <section class="news-block">
		        <div class="container">
		            <div class="news-block__row">

		                <div class="news-block__group">
		                    <div class="h3"><a href="#">Информация</a></div>
		                    <ul class="news-block__list">
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Еще раз об огнетушащей эффективности 3F пенообразователей</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Огнетушащие свойства безфтористых пенообразователей для пожаротушения типа 3F: Сравнение с пленкообразующими AFFF пенообразователями</a></h4>
		                        </li>
		                    </ul>
		                </div>

		                <div class="news-block__group">
		                    <div class="h3"><a href="#">Отзывы</a></div>
		                    <ul class="news-block__list">
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ»</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Отзыв на пенообразователь ПО-НСВ от ОАО «Международный аэропорт Шереметьево»</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Отзыв на пенообразователь ПО-НСВ от ООО «РН-Пожарная безопасность»</a></h4>
		                        </li>
		                    </ul>
		                </div>

		                <div class="news-block__group">
		                    <div class="h3"><a href="#">Новости компании</a></div>
		                    <ul class="news-block__list">
		                        <li>
		                            <h4 class="news-block__title"><a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">ООО «ЭГИДА ПТВ» приняло участие в международном салоне «Комплексная безопасность 2015»</a></h4>
		                        </li>
		                        <li>
		                            <h4 class="news-block__title"><a href="#">ЗАО «ЭГИДА ПТВ» реорганизованно в ООО «ЭГИДА ПТВ»</a></h4>
		                        </li>
		                    </ul>
		                </div>

		            </div>
		        </div>
		    </section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
