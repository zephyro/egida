<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="main_header main_header_gradient">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li>Пресс-центр</li>
						</ul>
					</div>
				</div>
				<div class="header_content">
					<div class="container">
						<div  class="h1">Пресс-центр</div>
					</div>
				</div>
			</div>

		    <section class="main">
			    <div class="container container_long">
					<div class="main__row">

						<div class="main__sidebar">
							<nav class="sidenav sticky">
								<ul class="sidenav_first">
									<li class="open">
										<a href="#"><span><i>ПРЕСС-ЦЕНТР</i></span></a>

										<ul class="sidenav_news">
											<li>
												<span class="sidenav_news__date">14.02.2018</span>
												<a href="#">ООО «ЭГИДА ПТВ» приняло участие в международном салоне «Комплексная безопасность 2015»</a>
											</li>
											<li>
												<span class="sidenav_news__date">14.02.2018</span>
												<a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a>
											</li>
											<li>
												<span class="sidenav_news__date">14.02.2018</span>
												<a href="#">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</a>
											</li>
										</ul>

									</li>
									<li>
										<a href="#"><span><i>СОБЫТИЯ ОТРАСЛИ</i></span></a>
									</li>
								</ul>
							</nav>
						</div>

						<div class="main__content">

							<div class="news_header">
								<div class="news_header__text">
									<h1 class="news_title">ООО «ЭГИДА ПТВ» приняло участие в международном салоне «Комплексная безопасность 2015»</h1>
									<div class="news_date">14.02.2018</div>
								</div>
							</div>

							<div class="news_content">


								<div class="news_author news_author_right">
									<div class="news_author__col">
										<div class="news_author__photo">
											<img src="img/news__author.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_author__col">
										<div class="news_author__photo">
											<img src="img/news__author.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_author__col_text">
										<div class="news_author__text">
											<div>Авторы статьи:  В.Ю.Гаравин, А.В.Третьяков</div>
											<div>ООО «ЭГИДА ПТВ»</div>
										</div>
									</div>
								</div>

								<p>С 19 по 22 мая 2015 г. на территории ВДНХ, в павильоне № 75 прошел международный салон «Комплексная безопасность 2015». В выставке приняли участие более 450 представителей организаций, 48 официальных делегаций и более 14.000 посетителей. Традиционно в выставочной экспозиции приняло участие и ООО «ЭГИДА ПТВ». Помимо обновленной продукции и комплексных решений в сфере пожаротушения, ЭГИДА ПТВ представила «Методические рекомендации по применению систем пенного пожаротушения».</p>
								<p>С электронной версией методики применения вы можете ознакомиться на нашем сайте в разделе «Методика применения».</p>

								<div class="news_gallery">

									<div class="news_gallery__mobile">

										<div class="news_gallery__mobile_item">
											<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery02">
												<div class="image_item__content">
													<img src="img/news__image.jpg" class="img-fluid" alt="">
												</div>
												<span class="image_item__loop"></span>
											</a>
										</div>

										<div class="news_gallery__mobile_item">
											<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery02">
												<div class="image_item__content">
													<img src="img/news__image.jpg" class="img-fluid" alt="">
												</div>
												<span class="image_item__loop"></span>
											</a>
										</div>

										<div class="news_gallery__mobile_item">
											<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery02">
												<div class="image_item__content">
													<img src="img/news__image.jpg" class="img-fluid" alt="">
												</div>
												<span class="image_item__loop"></span>
											</a>
										</div>

									</div>

									<div class="news_gallery__desktop">

										<div class="news_gallery__row">

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

											<div class="news_gallery__col">
												<a href="img/news__image_large.jpg" class="image_item" data-fancybox="gallery01">
													<div class="image_item__content">
														<img src="img/news__image.jpg" class="img-fluid" alt="">
													</div>
													<span class="image_item__loop"></span>
												</a>
											</div>

										</div>

									</div>

								</div>

								<p>С 19 по 22 мая 2015 г. на территории ВДНХ, в павильоне № 75 прошел международный салон «Комплексная безопасность 2015». В выставке приняли участие более 450 представителей организаций, 48 официальных делегаций и более 14.000 посетителей. Традиционно в выставочной экспозиции приняло участие и ООО «ЭГИДА ПТВ». Помимо обновленной продукции и комплексных решений в сфере пожаротушения, ЭГИДА ПТВ представила «Методические рекомендации по применению систем пенного пожаротушения». </p>
								<p>С электронной версией методики применения вы можете ознакомиться на нашем сайте в разделе «Методика применения».</p>

								<br/>

								<div class="news_image">
									<div class="news_image__item">
										<img src="img/news__image_large.jpg" class="img-fluid" alt="">
									</div>
									<div class="news_image__text">Картинка в тексте. Подпись к картинке</div>
								</div>

								<div class="table_header">
									<span>Таблица 1.</span>
									<strong>Результаты демонстрационных испытаний пенообразователей компании Solberg на соответствие требованиям стандарта ICAO Level B (SAA-IAFPA foam Seminar, 20-22 июля, 2016, Сингапур).</strong>
								</div>

								<div class="table_responsive">
									<table class="table">
										<tr>
											<th>Дата</th>
											<th>Наименование</th>
											<th>Температура/Влажность</th>
											<th>Время тушения <div class="hide-xs hide-sm hide-md">(норма не более 60 с)</div></th>
										</tr>
										<tr>
											<td>19 июля 2016</td>
											<td>3F RE-HEALING™Foam</td>
											<td>33°С/76%</td>
											<td>>120 секунд</td>
										</tr>
										<tr>
											<td>19 июля 2016</td>
											<td>3F RE-HEALING™Foam</td>
											<td>33°С/76%</td>
											<td>>120 секунд</td>
										</tr>
										<tr>
											<td>19 июля 2016</td>
											<td>Пенообразователь AFFF на фтор ПАВ</td>
											<td>33°С/76%</td>
											<td>>120 секунд</td>
										</tr>
									</table>
								</div>

								<p>В табл. 1 приведены физико-технические характеристики неавтоклавного пенобетона, который мы производили в условиях стройплощадки с применением местных материалов. Из табл. 1 видно, что использовать пенобетон неавтоклавного твердения можно как:</p>

								<ul class="content_list">
									<li>конструкционный;</li>
									<li>конструкционно-теплоизоляционный;</li>
									<li>теплоизоляционный материал.</li>
								</ul>

								<p><strong>Результаты демонстрационных испытаний пенообразователей компании Solberg на соответствие требованиям стандарта ICAO Level B (SAA-IAFPA foam Seminar, 20-22 июля, 2016, Сингапур).</strong></p>

								<div class="blue_box">
									С 19 по 22 мая 2015 г. на территории ВДНХ, в павильоне № 75 прошел международный салон «Комплексная безопасность 2015». В выставке приняли участие более 450 представителей организаций, 48 официальных делегаций и более 14.000 посетителей. Традиционно в выставочной экспозиции приняло участие и ООО «ЭГИДА ПТВ». Помимо обновленной продукции и комплексных решений в сфере пожаротушения, ЭГИДА ПТВ представила «Методические рекомендации по применению систем пенного пожаротушения».
								</div>

								<div class="color-dark-blue mb-40"><strong>Пенообразователи поставляются в пластиковых емкостях объемом 200 или 1000 литров следующего вида:</strong></div>

								<div class="box_overflow">
									<div class="box_row">
										<div class="box_row__left">
											<div class="content_product">
												<div class="content_product__image">
													<img src="img/content_product__01.png" class="img-fluid" alt="">
												</div>
												<div class="content_product__description">
													<ul>
														<li>Диаметр 580 мм</li>
														<li>Высота 995 мм</li>
														<li>Вес 10 кг</li>
														<li>Объем 200 л</li>
														<li>Объем тары 0.26 м3</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="box_row__right">
											<div class="content_product">
												<div class="content_product__image">
													<img src="img/content_product__01.png" class="img-fluid" alt="">
												</div>
												<div class="content_product__description">
													<ul>
														<li>Диаметр 580 мм</li>
														<li>Высота 995 мм</li>
														<li>Вес 10 кг</li>
														<li>Объем 200 л</li>
														<li>Объем тары 0.26 м3</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="news_image">
									<a href="img/news__image_large.jpg" class="news_image__link" data-fancybox="single">
										<img src="img/news__image_large.jpg" class="img-fluid" alt="">
										<span class="news_image__loop"></span>
									</a>
									<div class="news_image__text">Картинка в тексте. Подпись к картинке</div>
								</div>

							</div>

						</div>

					</div>
			    </div>
		    </section>

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Modal Order -->
		<div class="hide">
			<div class="modal" id="request">
				<div class="modal__title color-orange">Отправить запрос</div>
				<div class="modal__product">Пенообразователь «Транспортный» НСВ</div>
				<form class="form">

					<div class="form_row">
						<div class="form_col_55">
							<input type="text" class="form-control" name="" placeholder="Объём пенообразователя*">
						</div>
						<div class="form_col_45">
							<select class="form-select">
								<option value="">ед. изм.</option>
								<option value="мм.">мм.</option>
								<option value="см.">см.</option>
								<option value="м.">м.</option>
								<option value="км.">км.</option>
							</select>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Концентрация пенообразователя">
								<span class="form_legend_text">%</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Температура замерзания">
								<span class="form_legend_text">C</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваше имя*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Организация">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваш телефон">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="E-mail*">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-orange btn-send">Отправить</button>
					</div>
				</form>
			</div>
		</div>
		<!-- -->

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
