
$(function() {

    $('.header__toggle').on('click', function(e) {
        e.preventDefault();
        $('.page').removeClass('open-modal');
        $('.page').toggleClass('open-nav');
    });


    $('.header__order_mobile').on('click', function(e) {
        e.preventDefault();
        $('.page').removeClass('open-nav');
        $('.page').toggleClass('open-modal');
    });


    $('.header__search_toggle').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.header__search').toggleClass('open');
    });


    if ($(event.target).closest(".product-properties").length === 0) {
        $(".product-properties").removeClass('open');
    }

    $('body').click(function (event) {

        if ($(event.target).closest(".header__nav").length === 0) {

            if ($(event.target).closest(".header__toggle").length === 0) {
                $(".page").removeClass('open-nav');
            }
        }

        if ($(event.target).closest(".header__modal").length === 0) {

            if ($(event.target).closest(".header__order_mobile").length === 0) {
                $(".page").removeClass('open-modal');
            }
        }
    });

});

$('.btn-top').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500);
    return false;
});



$(".btn-modal").fancybox({
    'padding'    : 0
});


$('.slider_main').slick({
    prevArrow: '<span class="icon-arrow_slider index-prev"><span class="path1"></span><span class="path2"></span></span>',
    nextArrow: '<span class="icon-arrow_slider index-next"><span class="path1"></span><span class="path2"></span></span>',
    dots: true,
    fade: true,
    cssEase: 'linear',
    responsive: [{
        adaptiveHeight: true,
        breakpoint: 500,
        settings: {
            arrows: false
        }
    }]
});



$('.comments__slider').slick({
    prevArrow: '<span class="slide_arrow slide_prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide_arrow slide_next"><i class="fa fa-angle-right"></i></span>',
    dots: true,
    fade: true,
    cssEase: 'linear',
    responsive: [{
        adaptiveHeight: true,
        breakpoint: 500,
        settings: {
            arrows: false
        }
    }]
});

// Side Nav

$(".sidenav .dropdown > a").click(function (){

    $(this).closest('li').toggleClass('open');

});




var pr = value = window.devicePixelRatio;
var w =$(window).width();

console.log(pr);

if (w > 768) {

    // Находим плавающий блок и делаем его плавающим
    $('.sticky').stick_in_parent({
        // Чтобы сразу прокручивался обратно
        inner_scrolling: true,
        // Отступ сверху
        offset_top: 120
    });
}



$('.cat__header').on('click', function(e) {
    e.preventDefault();
    $(this).closest('.cat').toggleClass('open');
});


tippy('[rel="tooltip"]', {
    delay: 100,
    arrow: true,
    theme: 'light',
    arrowType: 'round',
    size: 'large',
    duration: 500,
    animation: 'fade'
})


$('.news_gallery__mobile').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="slide_arrow slide_prev"><i class="fa fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide_arrow slide_next"><i class="fa fa-chevron-right"></i></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});