<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="about_top">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li>О компании</li>
						</ul>
					</div>
				</div>
				<div class="container">
					<h1 class="about_top__header">
						<span>О Компании</span>
						<strong>ЭГИДА ПТВ</strong>
					</h1>
					<div class="about_top__text">Новый уровень огнетушащей эффективности и качества тушения горючих жидкостей</div>
					<div class="about_top__image">
						<div class="about_top__image_wrap">
							<img src="img/about__image.jpg" class="img-fluid" alt="">
						</div>
					</div>
				</div>
			</div>

			<section class="main main_about">
				<div class="container">

					<div class="progress progress_left">
						<div class="progress__first">
							<img src="img/icon__circles.png" class="img-fluid" alt="">
							<div class="progress__first_text">
								<strong>1990</strong>
								<span>год основания</span>
							</div>
						</div>
						<div class="progress__text">
							<div class="progress__text_wrap">
								ООО «ЭГИДА ПТВ» было основано в 1998 году с целью разработки и производства современных пенообразователей и смачивателей для тушения пожаров, а также специальных составов на основе поверхностно-активных веществ. Обладая мощной научной и производственной базой и реализуя полный цикл производства современных огнетушащих средств, начиная от разработки состава пенообразователя, постановки на производство и заканчивая послегарантийным обслуживанием, ООО «ЭГИДА ПТВ» может предложить своим клиентам лучшие условия на рынке по цене на пенообразователь.
							</div>
						</div>
					</div>

					<div class="progress progress_right">
						<div class="progress__heading">
							<div class="progress__heading_row">
								<div class="progress__heading_col">
									<strong>Первые<br/>в России</strong>
									<span>освоили производство пенообразователей AFFF</span>
								</div>
							</div>
						</div>
						<div class="progress__text">
							<div class="progress__text_wrap">
								В настоящее время мы производим все типы пенообразователей общего и целевого назначения и смачиватели, которые применяются при тушении пожаров в различных отраслях народного хозяйства.  ООО «ЭГИДА ПТВ» – первое предприятие в России, которое освоило производство пенообразователей типа AFFF. За счёт наличия в составе пенообразователя фторированных поверхностно-активных веществ они вышли на новый уровень огнетушащей эффективности и качества тушения горючих жидкостей.
							</div>
						</div>
					</div>

					<div class="progress progress_left">
						<div class="progress__heading">
							<div class="progress__heading_row">
								<div class="progress__heading_col">
									<strong>Первые<br/>в мире</strong>
									<span>разработали и запатентовали состав пенообразователя общего назначения с 1%  рабочей концентрацией</span>
								</div>
							</div>
						</div>

						<div class="progress__text">
							<div class="progress__text_wrap">
								Мы первые в мире в 2005 году разработали и запатентовали состав пенообразователя общего назначения с 1% рабочей концентрацией, в 2010 году мы выпустили опытную партию  синтетического пенообразователя в твердом виде. Смачиватель, выпускаемый ООО «ЭГИДА ПТВ», по заключению ведущих специалистов пожарной охраны по своим техническим и эксплуатационным параметрам превосходит лучшие зарубежные аналоги. Мы первые подтвердили соответствие своих пенообразователей требованиям ГОСТ Р 50588-2012, а также разработали и внедрили в производство отечественный пенообразователь типа AFFF/AR-LV.
							</div>
						</div>
					</div>

					<div class="progress progress_right">
						<div class="progress__heading">
							<div class="progress__heading_row">
								<div class="progress__heading_col">
									<strong>
										Мы первые<br/>
										разработали<br/>
										и внедрили<br/>
										в производство
									</strong>
									<span>отечественный пенообразователь</span>
									<b>AFFF/AR-LV</b>
								</div>
							</div>
						</div>
						<div class="progress__text">
							<div class="progress__text_wrap">
								Мы стремимся быть первыми всегда и во всём. И для решения данной задачи наш коллектив выполняет колоссальный объём научно-исследовательских, опытно-конструкторских и поисковых работ, которые на первый взгляд не видны нашим заказчикам. Но они видят результаты наших усилий, приобретая и применяя наши продукты для решения своих зачастую непростых задач.</div>
						</div>
					</div>
				</div>
			</section>

			<div class="about_bottom">
				<div class="container">
					<div class="about_bottom__text">ООО «ЭГИДА ПТВ» приглашает Вас к сотрудничеству и обязуется сделать Вам предложение с оптимальным соотношением стоимость пенообразователя/его качество.</div>
				</div>
			</div>

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>