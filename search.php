<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="main_header main_header_gradient">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li>Поиск по сайту</li>
						</ul>
					</div>
				</div>
				<div class="header_content">
					<div class="container">
						<h1>Поиск по сайту</h1>
					</div>
				</div>
			</div>

		    <section class="main">
			    <div class="container">
				    <div class="search">
					    <form class="form">
						    <div class="search__input">
							    <input type="text" class="form-control" name="" placeholder="Поиск по сайту…">
						    </div>
						    <button type="submit" class="search__btn">Искать</button>
					    </form>
				    </div>

				    <div class="search__summary">Найдено 17 страниц.</div>


				    <ul class="search__result">

					    <li>
						    <h4><a href="#">Пенообразователь «Полярный LV»</a></h4>
						    <a class="search__result_link" href="#">http://test.dustnet.ru/production/</a>
					    </li>

					    <li>
						    <h4><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ» </a></h4>
						    <a class="search__result_link" href="#">http://test.dustnet.ru//otzyv-o-primenen…rneftegaz/</a>
					    </li>

					    <li>
						    <h4><a href="#">Пенообразователь «ПБ-Формула 2012» - отечественный инновационный продукт</a></h4>
						    <a class="search__result_link" href="#">http://test.dustnet.ru/information/publi…j-produkt/</a>
					    </li>

					    <li>
						    <h4><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ» </a></h4>
						    <a class="search__result_link" href="#">http://test.dustnet.ru//otzyv-o-primenen…rneftegaz/</a>
					    </li>

					    <li>
						    <h4><a href="#">Пенообразователь «ПБ-Формула 2012» - отечественный инновационный продукт</a></h4>
						    <a class="search__result_link" href="#">http://test.dustnet.ru/information/publi…j-produkt/</a>
					    </li>

				    </ul>

				    <ul class="pagination text-center">
					    <li><a href="#">1</a></li>
					    <li><a href="#">2</a></li>
					    <li><a href="#">3</a></li>
					    <li><a href="#">4</a></li>
					    <li><a href="#">5</a></li>
					    <li><a href="#">→</a></li>
				    </ul>

			    </div>
		    </section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
