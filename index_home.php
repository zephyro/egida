<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<!-- Slider -->
			<div class="slider_main">
				<div class="slider_main__elem" style="background-image: url('images/slider_01.jpg')">
					<div class="slider_main__row">
						<div class="slider_main__col">
							<div class="container">

								<div class="slider_main__title">ПЕНООБРАЗОВАТЕЛИ</div>
								<a href="#" class="btn-orange">Подробнее</a>
							</div>
						</div>
					</div>
					<div class="slider_main__lines">
						<img src="img/slides_lines.png" class="img-fluid" alt="">
					</div>
				</div>
				<div class="slider_main__elem" style="background-image: url('images/slider_02.jpg')">
					<div class="slider_main__row">
						<div class="slider_main__col">
							<div class="container">

								<div class="slider_main__title">ПЕНООБРАЗОВАТЕЛИ</div>
								<a href="#" class="btn-orange">Подробнее</a>
							</div>
						</div>
					</div>
					<div class="slider_main__lines">
						<img src="img/slides_lines.png" class="img-fluid" alt="">
					</div>
				</div>
				<div class="slider_main__elem" style="background-image: url('images/slider_03.jpg')">
					<div class="slider_main__row">
						<div class="slider_main__col">
							<div class="container">

								<div class="slider_main__title">ПЕНООБРАЗОВАТЕЛИ</div>
								<a href="#" class="btn-orange">Подробнее</a>
							</div>
						</div>
					</div>
					<div class="slider_main__lines">
						<img src="img/slides_lines.png" class="img-fluid" alt="">
					</div>
				</div>
			</div>
			<!-- End Slider -->

			<!-- Advantages -->
			<section class="advantage">
				<div class="container">
					<div class="advantage__box">
						<div class="advantage__row">

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__01.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>ФТОРСИНТЕТИЧЕСКИЕ  ПЕНООБРАЗОВАТЕЛИ</span></div>
								</a>
							</div>

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__02.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>СПИРТОУСТОЙЧИВЫЕ ПЕНООБРАЗОВАТЕЛИ</span></div>
								</a>
							</div>

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__03.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>СИНТЕТИЧЕСКИЕ УГЛЕВОДОРОДНЫЕ ПЕНООБРАЗОВАТЕЛИ</span></div>
								</a>
							</div>

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__04.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>СМАЧИВАТЕЛИ</span></div>
								</a>
							</div>

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__05.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>ПЕНООБРАЗОВАТЕЛИ БЕНЗИНОФФ</span></div>
								</a>
							</div>

							<div class="advantage__col">
								<a href="#" class="advantage__elem">
									<div class="advantage__elem_icon">
										<img src="img/advantage__06.svg" class="img-fluid" alt="">
									</div>
									<div class="advantage__elem_title"><span>ПЕНООБРАЗОВАТЕЛИ  ДЛЯ ПЕНОБЕТОНА</span></div>
								</a>
							</div>


						</div>
					</div>
				</div>
			</section>
			<!-- End Advantages -->

			<!-- About -->
			<section class="about">
				<div class="about__line">
					<img src="img/top_lines.svg" class="img-fluid" alt="">
				</div>
				<div class="container">
					<div class="about__image">
						<div class="about__image_wrap">
							<img src="img/about__image.jpg" class="img-fluid" alt="">
						</div>
					</div>
					<div class="about__content hide_box_container">
						<h1>Пенообразователь пожарный от ООО «ЭГИДА ПТВ»</h1>
						<p>Одним из приоритетных направлений в пожаротушении являются пенные системы пожаротушения. Это обусловлено тем, что до сегодняшнего дня наиболее эффективным и, пожалуй, единственным, способом борьбы с пожарами нефти и нефтепродуктов является пожарная пена.</p>
						<p>Учитывая то, что топливно-энергетический комплекс является одной из стратегических основ экономики любого государства, обеспечение его безопасности, и в частности пожарной, является особо важной задачей, требующей постоянного повышенного внимания. Современный пенообразователь для тушения пожаров позволяет решить фактически любые задачи, связанные с целями пожаротушения на опасных производственных объектах. </p>

						<div class="hide_box">
							<p>Исследования и создание новых высокоэффективных средств пенного пожаротушения являются одним из важнейших направлений работ специалистов пожарной безопасности. ООО «ЭГИДА ПТВ» - первая компания в России, которая освоила серийный выпуск пенообразователей для пожаротушения типа AFFF.
								Собственное производство пенообразователя и высокий научный потенциал компании позволяет разрабатывать и выпускать весь спектр пенообразователей для пожаротушения с оптимальным соотношением цена/качество. </p>
							<p>
								В ходе плановой модернизации пенообразователей в 2013 году, ООО «ЭГИДА-ПТВ» выпустила обновленную линейку пожарной пены. Новые модифицкации пенообразователей полностью соответствует требованиям ГОСТ Р 50588-2012 и ГОСТ Р 53280.2-2010. В частности, началось производство и продажа следующих пенообразователей:<br/>
								Пенообразователь фторсинтетический пленкоообразующий ПО-А3F;<br/>
								Пенообразователь фторсинтетический пленкообразующий Меркуловский;<br/>
								Синтетический фторсодержащий спиртоустойчивый пенообразователь Полярный/Полярный LV;<br/>
								Синтетический пенообразователь ПО-НСВ («Море», «Авиа», «Люкс»);<br/>
								Синтетический пенообразователь «Транспортный» НСВ.<br/>
							</p>
						</div>
						<div class="hide_box_btn">
							<a href="#" class="btn-orange">Подробнее</a>
						</div>
					</div>
				</div>
			</section>
			<!-- End About -->

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Video -->
			<section class="video_block">
				<div class="container">
					<div class="h1 text-center">Видео-презентация ООО «ЭГИДА ПТВ»</div>
					<div class="video_block__wrap">
						<a data-fancybox="" class="video_block__elem" href="http://www.youtube.com/watch?v=2JaQSQHclk0">
							<img src="img/video.jpg" alt="" class="img-fluid">
						</a>
					</div>
				</div>
			</section>
			<!-- End Video -->

			<!-- Comments -->
			<section class="comments">
				<div class="container">
					<div class="h1 text-center">Отзывы о пенообразователях</div>

					<div class="comments__slider_wrap">
						<div class="comments__slider">

							<div class="comments__slider_item">
								<div class="comments__item">
									<div class="comments__item_image">
										<div class="comments__item_image_wrap">
											<img src="img/review.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="comments__item_body">
										<div class="comments__item_title"><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ</a></div>
										<div class="comments__item_text">
											17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
											тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе
											учебного полигона в пгт. Ахтырский, руководством
											ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны
											фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ»
											промышленной партии № 52 общим объемом 13 тонн.
										</div>
										<a href="#" class="btn-orange">Подробнее</a>
									</div>
								</div>
							</div>

							<div class="comments__slider_item">
								<div class="comments__item">
									<div class="comments__item_image">
										<div class="comments__item_image_wrap">
											<img src="img/review.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="comments__item_body">
										<div class="comments__item_title"><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ</a></div>
										<div class="comments__item_text">
											17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
											тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе
											учебного полигона в пгт. Ахтырский, руководством
											ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны
											фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ»
											промышленной партии № 52 общим объемом 13 тонн.
										</div>
										<a href="#" class="btn-orange">Подробнее</a>
									</div>
								</div>
							</div>

							<div class="comments__slider_item">
								<div class="comments__item">
									<div class="comments__item_image">
										<div class="comments__item_image_wrap">
											<img src="img/review.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="comments__item_body">
										<div class="comments__item_title"><a href="#">Отзыв о применении пенообразователя ПО-6А3F «РН-КРАСНОДАРНЕФТЕГАЗ</a></div>
										<div class="comments__item_text">
											17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
											тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе
											учебного полигона в пгт. Ахтырский, руководством
											ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны
											фторсинтетического пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ»
											промышленной партии № 52 общим объемом 13 тонн.
										</div>
										<a href="#" class="btn-orange">Подробнее</a>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="text-center hide-sm-only hide-md-only hide-lg-only hide-xl">
						<a href="#" class="btn-orange">Все отзывы</a>
					</div>

				</div>
			</section>
			<!-- End Comments -->

			<!-- News -->
			<section class="news">
				<div class="container">
					<div class="h1 text-center">Новости</div>

					<div class="news__row">

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

						<div class="news__col">
							<div class="news_item">
								<a class="news_item__post" href="">
									<div  class="news_item__image">
										<div class="news_item__image_wrap">
											<img src="img/news-item.jpg" class="img-fluid" alt="">
										</div>
									</div>
									<div class="news_item__title">Новая уникальная научная работа о пенообразователях для пожаротушения от ООО «ЭГИДА ПТВ»</div>
								</a>
								<div class="news_item__date">14.02.2018</div>
								<div class="news_item__tag">
									<a href="#">Новости компании</a>
								</div>
								<div class="news_item__intro">
									17 августа 2012 года на базе ООО «РН-Краснодарнефтегаз» Для обеспечения проведения
									тактико-специального учения по ликвидации аварии на объекте нефтедобычи (РВС-1000) на базе учебного
									полигона в пгт. Ахтырский, руководством
									ООО «РН-Краснодарнефтегаз» было принято решение о выделении 1 (одной) тонны фторсинтетического
									пенообразователя ПО-6А3F (6%) из ранее закупленной у ООО «ЭГИДА ПТВ» промышленной партии
									№ 52 общим объемом 13 тонн.
								</div>
								<a class="news_item__link" href="#">Подробнее</a>
							</div>
						</div>

					</div>

					<div class="text-center">
						<a href="#" class="btn-orange">Все новости</a>
					</div>

				</div>
			</section>
			<!-- End News -->

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
