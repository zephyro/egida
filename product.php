<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="main_header main_header_goods">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li><a href="#">Продукция</a></li>
							<li><a href="#">Пенообразователи</a></li>
							<li><a href="#">Синтетические углеводородные пенообразователи</a></li>
							<li>Пенообразо­ватель «Транспортный» НСВ</li>
						</ul>
					</div>
				</div>
			</div>

		    <section class="main">
			    <div class="container container_long">
					<div class="main__row">

						<div class="main__sidebar">
							<nav class="sidenav sticky">
								<ul class="sidenav_first">
									<li class="dropdown active open">
										<a href="#"><span><i>ПЕНООБРАЗОВАТЕЛИ ДЛЯ ПОЖАРОТУШЕНИЯ</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown open">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li class="active">
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#"><span><i>ППЕНООБРАЗОВАТЕЛИ ДЛЯ ПЕНОБЕТОНА</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#"><span><i>ППОЖАРНО-ТЕХНИЧЕСКОЕ ОБОРУДОВАНИЕ</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
										<a href="#"><span><i>ПСМАЧИВАТЕЛИ</i></span></a>
									</li>
								</ul>
							</nav>
						</div>

						<div class="main__content">

							<div class="goods">

								<div class="goods__row">
									<div class="goods__header">
										<div class="goods__header_icon">
											<img src="img/advantage__03.svg" class="img-fluid" alt="">
										</div>
										<h1>Пенообразователь «Транспортный» НСВ</h1>
										<div class="text_center hide-xs-only hide-sm-only">
											<a href="#request" class="btn-orange btn_adaptive btn-modal">Отправить запрос</a>
										</div>
									</div>
									<div class="goods__text">
										<p>«Транспортный» НСВ – синтетический пенообразователь целевого назначения, предназначенный для получения пены низкой, средней и высокой кратности с использованием питьевой, жёсткой и морской воды при тушении нефти, нефтепродуктов, водонерастворимых горючих жидкостей, а также приготовления растворов смачивателей.</p>
										<p>В соответствии с ГОСТ Р 50588-2012 и международной классификацией пенообразователь «Транспортный» НСВ относится к типу S, при использовании в качестве смачивателя к типу WA.</p>
										<div class="goods__text_button hide-md-only hide-lg-only hide-xl">
											<a href="#request" class="btn-orange btn-modal">Отправить запрос</a>
										</div>
									</div>
								</div>

								<div class="tabs goods_tabs">

									<ul class="goods_tabs__nav">
										<li><a href="#tab_1"><span>Описание</span></a></li>
										<li><a href="#tab_2"><span>Технические характеристики</span></a></li>
										<li><a href="#tab_3"><span>Модификации</span></a></li>
										<li><a href="#tab_4"><span>Сертификаты</span></a></li>
										<li><a href="#tab_5"><span>Видео</span></a></li>
									</ul>

									<div class="goods_tabs__item" id="tab_1">
										<p>Пенообразователь не содержит в своём составе перфтороктансульфонатов  (PFOS), перфтороктановой кислоты (PFOA) и ее солей.</p>
										<p>Выпускаются модификации с объёмным содержанием концентрата пенообразователя в рабочем растворе 1%, 3% и 6%. Пенообразователь по своим параметрам соответствует ГОСТ Р 50588-2012 и ТУ 2412-002-49888190-2013</p>
										<p>ООО «ЭГИДА ПТВ» является единственным производителем пенообразователя ПО-6A3F, о чём свидетельствует <a href="#">Сертификат соответствия</a>.</p>
										<p>Сотрудничая с нами, вы всегда получаете самые льготные условия и гибкую ценовую политику. Подробности по телефону: +7(495) 787-42-81.</p>

										<ul class="goods_advantage">
											<li>
												<div class="goods_advantage__item">
													<span>Соответствует требованиям ГОСТ Р 50588-2012</span>
												</div>
											</li>
											<li>
												<div class="goods_advantage__item">
													<span>Соответствует требованиям ГОСТ Р 50588-2012</span>
												</div>
											</li>
											<li>
												<div class="goods_advantage__item">
													<span>Соответствует требованиям Российского морского регистра судоходства</span>
												</div>
											</li>
											<li>
												<div class="goods_advantage__item">
													<span>Соответствует требованиям Гражданской авиации</span>
												</div>
											</li>
										</ul>

										<h3 class="text-center">Пенообразователь ПО-6А3F может применяться в следующих случаях:</h3>

										<div class="goods__info">
											<ul>
												<li class="goods__info_01">
													<span>Для  тушения нефтепродуктов пеной низкой, средней и высокой кратности.</span>
												</li>
												<li class="goods__info_02">
													<span>Подачей низкократной пены в слой и на поверхность горящей жидкости (комбинированный способ пожаротушения).</span>
												</li>
												<li class="goods__info_03">
													<span>При тушении стабильного газового конденсата пеной низкой и средней кратности.</span>
												</li>
												<li class="goods__info_04">
													<span>При тушении бензинов с высоким октановым числом пеной низкой и средней кратности.</span>
												</li>
												<li class="goods__info_05">
													<span>Объёмном тушении пожаров пеной средней и высокой кратности.</span>
												</li>
												<li class="goods__info_06">
													<span>При тушении пожаров на морских и речных судах, нефтегазодобывающих установках, расположенных в акватории моря.</span>
												</li>
											</ul>
											<ul>
												<li class="goods__info_07">
													<span>При тушении пожаров на прибрежных объектах, в зоне размещения которых существует дефицит пресной воды, в районах с повышенным содержанием в воде солей кальция и магния.</span>
												</li>
												<li class="goods__info_08">
													<span>Для тушения наземных пожаров на воздушных судах.</span>
												</li>
												<li class="goods__info_09">
													<span>Для покрытия взлётно-посадочных полос пеной при аварийных посадках самолётов.</span>
												</li>
												<li class="goods__info_10">
													<span>При тушении пожаров стационарными установками с использованием распыливающих устройств (дренчеров и спринклеров).</span>
												</li>
												<li class="goods__info_11">
													<span>Для защиты строительных конструкций, технологических аппаратов и хранящихся материалов от воздействия тепловых потоков.</span>
												</li>
											</ul>
										</div>

									</div>

									<div class="goods_tabs__item" id="tab_2">
										<p>Phasellus eget orci sed libero porttitor pharetra. Donec at erat cursus nisi viverra molestie. Morbi condimentum felis nibh, ut euismod massa vestibulum at. Suspendisse vitae tincidunt nibh. Etiam orci nibh, accumsan consectetur magna quis, dictum semper elit. Sed placerat ipsum id ante aliquet iaculis. Duis id sem in mi condimentum aliquam. Curabitur hendrerit interdum nisi et ultrices. Vestibulum finibus ante in eleifend semper. Nam in venenatis tellus, vitae convallis urna. Nam non cursus ipsum.</p>
									</div>

									<div class="goods_tabs__item" id="tab_3">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim magna quam, eu bibendum odio malesuada nec. Vivamus ullamcorper euismod est, eget bibendum ipsum. Maecenas aliquet, elit et pellentesque tincidunt, urna magna sodales arcu, ut vestibulum metus ipsum vel turpis. Donec commodo sodales sem eu facilisis. Curabitur id venenatis urna. Phasellus sem quam, auctor condimentum pulvinar a, semper vel nisl. Sed in consequat ex, a pretium massa. Praesent vel vestibulum justo. Phasellus at gravida erat. In vulputate ut nibh ac sagittis. Integer finibus lorem et lacus dictum, quis dapibus lectus vestibulum. Donec elementum ornare elit, ut porttitor dolor mattis vel. In fermentum enim augue, quis commodo justo tincidunt ac.
									</div>

									<div class="goods_tabs__item" id="tab_4">
										<p>Morbi blandit est nunc, in euismod justo tincidunt in. Nullam gravida mauris vitae elementum fringilla. Pellentesque et tellus posuere, commodo magna ac, hendrerit nulla. Nullam enim felis, semper nec ipsum ut, hendrerit luctus nunc. Quisque quis venenatis turpis, vitae porta tellus. Ut eleifend sem turpis, et ullamcorper velit rhoncus ac. Duis eget vulputate dolor. Aliquam tincidunt erat at mauris suscipit luctus. Curabitur ullamcorper tellus sit amet nisi fermentum molestie.</p>
									</div>

									<div class="goods_tabs__item" id="tab_5">
										<p>Fusce sit amet urna id massa pellentesque tincidunt non vel sapien. Curabitur a volutpat turpis. Curabitur feugiat a enim eget feugiat. Fusce congue massa ultricies magna euismod, sit amet rhoncus tellus lacinia. Nullam lacus augue, aliquam bibendum euismod et, tempor in augue. Aenean tellus ipsum, cursus sed rutrum mattis, scelerisque eu risus. Vestibulum vestibulum venenatis leo vel accumsan. Ut tempus justo libero. Ut orci eros, tempus porta nibh nec, sodales vestibulum velit. Vivamus porttitor in ipsum sed ullamcorper. Phasellus faucibus dolor eu lacus tincidunt faucibus. Morbi dui mauris, pellentesque at aliquam et, rhoncus non dolor. Aliquam vestibulum pharetra eros eu tincidunt. Phasellus pulvinar, dolor id tristique laoreet, erat quam hendrerit dui, eu dignissim velit felis nec urna. Integer ut congue sem, et semper tortor.</p>
									</div>

								</div>

							</div>

						</div>

					</div>
			    </div>
		    </section>

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Modal Order -->
		<div class="hide">
			<div class="modal" id="request">
				<div class="modal__title color-orange">Отправить запрос</div>
				<div class="modal__product">Пенообразователь «Транспортный» НСВ</div>
				<form class="form">

					<div class="form_row">
						<div class="form_col_55">
							<input type="text" class="form-control" name="" placeholder="Объём пенообразователя*">
						</div>
						<div class="form_col_45">
							<select class="form-select">
								<option value="">ед. изм.</option>
								<option value="мм.">мм.</option>
								<option value="см.">см.</option>
								<option value="м.">м.</option>
								<option value="км.">км.</option>
							</select>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Концентрация пенообразователя">
								<span class="form_legend_text">%</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Температура замерзания">
								<span class="form_legend_text">C</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваше имя*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Организация">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваш телефон">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="E-mail*">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-orange btn-send">Отправить</button>
					</div>
				</form>
			</div>
		</div>
		<!-- -->

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

		<script>
            var $tabs = $('.tabs');

            $tabs.responsiveTabs({
                startCollapsed: 'accordion',
                collapsible: 'accordion'
            });

            $tabs.responsiveTabs('activate', 0);
		</script>

	</body>
</html>
