<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-1.9.1.min.js" integrity="sha256-wS9gmOZBqsqWxgIVgA8Y9WcQOa7PgSIX+rPA0VL2rbQ=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/responsive.tabs/js/jquery.responsiveTabs.min.js"></script>
<script src="js/vendor/jquery.scrollTo.min.js"></script>
<script src="js/vendor/tippy.js/tippy.min.js"></script>
<script src="js/vendor/slick/slick.min.js"></script>
<script src="js/vendor/jquery.sticky.js"></script>
<script src="js/main.js"></script>