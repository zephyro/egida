<header class="header">
    <div class="container-fluid">

        <a href="/" class="header__logo">
            <img src="img/logo.svg" class="svg-fluid" alt="">
        </a>

        <ul class="header__contact">
            <li><a href="tel:74957874282">+7(495) 787-42-82</a></li>
            <li><a href="tel:74957874282">+7(495) 787-42-82</a></li>
        </ul>

        <div class="header__order btn-round">
            <i class="icon-letter">
                <span class="path2"></span>
            </i>
            <a class="header__order_mobile" href="#"></a>
            <a class="header__order_desktop btn-modal "  href="#order"></a>
        </div>

        <a class="header__toggle btn-round open" href="#">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <div class="header__nav">
            <div class="header__nav_container">
                <nav class="topnav">
                    <ul>
                        <li class="active"><a href="#">О компании</a></li>
                        <li><a href="#">Продукция</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">Информация</a></li>
                        <li><a href="#">Новости</a></li>
                        <li><a href="#">Отзывы</a></li>
                        <li><a href="#">Вопросы/Ответы</a></li>
                        <li><a href="#">Контакты</a></li>
                    </ul>
                </nav>
                <div class="header__search">
                    <a href="#" class="header__search_toggle">
                        <i>
                            <img src="img/icon_search_white.svg" class="img-fluid" alt="">
                        </i>
                    </a>
                    <div class="header__search_form">
                        <form class="form">
                            <input class="header__search_input" type="text" name="" placeholder="Поиск по сайту">
                            <button type="submit" class="header__search_btn">
                                <i>
                                    <img src="img/icon_search_white.svg" class="img-fluid" alt="">
                                </i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="header__modal">
            <div class="header__modal_container">
                <div class="header__modal_title">Отправить запрос</div>
                <div class="header__modal_wrap">
                    <form class="form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Ваше имя*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Ваш телефон">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="E-mail*">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="" placeholder="Текст сообщения*" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-checkbox">
                                <input type="checkbox" name="" value="">
                                <span>Я согласен(а) на обработку моих персональных данных, в соответствии с <a href="#">Пользовательским соглашением</a> ООО «Эгида ПТВ»</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn-orange btn-send">Отправить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</header>