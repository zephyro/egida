
<footer class="footer">
    <div class="footer__main">
        <div class="container">

            <div class="footer__info">
                <div class="footer__logo">
                    <img src="img/logo.svg" class="img-fluid" alt="">
                </div>
                <ul class="footer__contact">
                    <li><span class="footer__contact_work">Пн-Пт с 8:00 - 18:00 (Мск)</span></li>
                    <li><a class="footer__contact_tel color_orange" href="tel:7(495) 7874282">+7(495) 787-42-82</a></li>
                    <li><a class="footer__contact_tel color_blue" href="tel:+74957874281">+7(495) 787-42-81</a></li>
                    <li>e-mail: <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></li>
                </ul>
            </div>

            <div class="footer__nav">

                <ul class="footer__nav_elem">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">О компании</a></li>
                    <li><a href="#">Продукция</a></li>
                    <li><a href="#">Услуги</a></li>
                    <li class="footer__nav_mobile"><a href="#">Контакты</a></li>
                </ul>

                <ul class="footer__nav_elem">
                    <li><a href="#">Информация</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Отзывы</a></li>
                    <li><a href="#">Вопросы/Ответы</a></li>
                    <li class="footer__nav_mobile"><a href="#">Карта сайта</a></li>
                </ul>

                <ul class="footer__nav_elem">
                    <li><a href="#">КОНТАКТЫ</a></li>
                    <li><a href="#">КАРТА САЙТА</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer__privacy">
        <div class="container">
            <a href="#">Пользовательское соглашение</a>
        </div>
    </div>
    <div class="footer__copy">
        <div class="container">ООО ЭГИДА ПТВ. Все права защищены. Копирование материалов сайта запрещено</div>
    </div>

    <a class="btn-top" href="#top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33 33" class="ico-svg">
            <g transform="translate(-332 -3582)">
                <path id="Path_64" data-name="Path 64" class="cls-1" d="M16.5,33A16.5,16.5,0,1,1,33,16.5,16.5,16.5,0,0,1,16.5,33ZM27.223,19.533a1.358,1.358,0,0,0,0-1.938L17.475,7.847a1.358,1.358,0,0,0-1.938,0L5.789,17.6a1.358,1.358,0,0,0,0,1.938l2.19,2.19a1.358,1.358,0,0,0,1.938,0l6.6-6.6,6.6,6.6a1.358,1.358,0,0,0,1.938,0Z" transform="translate(332 3582)"/>
            </g>
        </svg>
    </a>

</footer>

<!-- Modal -->
<div class="hide">
    <div class="modal" id="order">
        <div class="modal__title">Отправить запрос</div>
        <form class="form">
            <div class="form-group">
                <input type="text" class="form-control" name="" placeholder="Ваше имя*">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="" placeholder="Ваш телефон">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="" placeholder="E-mail*">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="" placeholder="Текст сообщения*" rows="6"></textarea>
            </div>
            <div class="form-group">
                <label class="form-checkbox">
                    <input type="checkbox" name="" value="">
                    <span>Я согласен(а) на обработку моих персональных данных, в соответствии с <a href="#">Пользовательским соглашением</a> ООО «Эгида ПТВ»</span>
                </label>
            </div>
            <div class="text-center">
                <button type="submit" class="btn-orange btn-send">Отправить</button>
            </div>
        </form>
    </div>
</div>
<!-- -->