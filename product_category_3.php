<!doctype html>
	<html class="no-js" lang="">

		<!-- Head -->
		<?php include('inc/head.inc.php') ?>
		<!-- -->

	<body id="top">

		<div class="page">

			<!-- Header -->
		    <?php include('inc/header.inc.php') ?>
			<!-- -->

			<div class="main_header main_header_product">
				<div class="breadcrumbs">
					<div class="breadcrumbs__container">
						<ul class="breadcrumb">
							<li><a href="#">Главная</a></li>
							<li><a href="#">Продукция</a></li>
							<li><a href="#">Пенообразователи</a></li>
							<li><a href="#">Синтетические углеводородные пенообразователи</a></li>
							<li>Пенообразо­ватель «Транспортный» НСВ</li>
						</ul>
					</div>
				</div>
			</div>

		    <section class="main">
			    <div class="container container_long">
					<div class="main__row">

						<div class="main__sidebar">
							<nav class="sidenav sticky">
								<ul class="sidenav_first">
									<li class="dropdown active open">
										<a href="#"><span><i>ПЕНООБРАЗОВАТЕЛИ ДЛЯ ПОЖАРОТУШЕНИЯ</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown open">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li class="active">
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#"><span><i>ППЕНООБРАЗОВАТЕЛИ ДЛЯ ПЕНОБЕТОНА</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li class="dropdown">
										<a href="#"><span><i>ППОЖАРНО-ТЕХНИЧЕСКОЕ ОБОРУДОВАНИЕ</i></span></a>
										<ul class="sidenav_second">
											<li class="dropdown">
												<a href="#"><span><i>Фторсинтетические</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Спиртоустойчивые</i></span></a>
												<ul>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
											<li class="dropdown">
												<a href="#"><span><i>Синтетические углеводородные</i></span></a>
												<ul class="sidenav_third">
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
													<li>
														<a href="#"><span>Пенообразователь «Транспортный» НСВ</span></a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<li>
										<a href="#"><span><i>ПСМАЧИВАТЕЛИ</i></span></a>
									</li>
								</ul>
							</nav>
						</div>

						<div class="main__content">

							<div class="category">
								<div class="category__header">
									<div class="category__header_icon">
										<img src="img/icon__product_03.svg" class="img-fluid" alt="">
									</div>
									<h1><span>Пенообразователи для пенобетона</span></h1>
									<div class="category__header_text">Краткое описание категории «Пенообразователи для производства пенобетона». Пенообразователи для производства пенобетона Пенообразователи для производства пенобетона</div>
								</div>
								<div class="category__content">

									<div class="product_elem__wrap">
										<a class="product_elem elem_gray" href="#">
											<div class="product_elem__icon">
												<div class=product_elem__icon_item">
													<img src="img/icon__goods_01.svg" class="img-fluid" alt="">
												</div>
											</div>
											<div class="product_elem__text">Пенообразователь для пенобетона «ПБ-Формула 2012»</div>
										</a>
									</div>

								</div>
							</div>

						</div>

					</div>
			    </div>
		    </section>

			<section class="contact-info">
				<div class="container">
					<div class="contact-info__text">Более подробно о пенообразователях вы можете узнать по телефону:</div>
					<ul class="contact-info__phone">
						<li>
							<a href="tel:74957874281"><i class="icon-phone"></i> <span>+7(495) 787-42-81</span></a>
						</li>
						<li>
							<a href="tel:74957874282"><i class="icon-phone"></i> <span>+7(495) 787-42-82</span></a>
						</li>
					</ul>
					<div class="contact-info__text"><span>Отправить запрос по e-mail:</span> <a href="mailto:info@egida-ptv.ru">info@egida-ptv.ru</a></div>
					<div class="text-center">
						<a href="#" class="btn-blue"><i class="icon-letter_white"></i> <span>Отправить запрос с сайта</span></a>
					</div>
				</div>
			</section>

			<!-- Footer -->
		    <?php include('inc/footer.inc.php') ?>
			<!-- -->

		</div>

		<!-- Modal Order -->
		<div class="hide">
			<div class="modal" id="request">
				<div class="modal__title color-orange">Отправить запрос</div>
				<div class="modal__product">Пенообразователь «Транспортный» НСВ</div>
				<form class="form">

					<div class="form_row">
						<div class="form_col_55">
							<input type="text" class="form-control" name="" placeholder="Объём пенообразователя*">
						</div>
						<div class="form_col_45">
							<select class="form-select">
								<option value="">ед. изм.</option>
								<option value="мм.">мм.</option>
								<option value="см.">см.</option>
								<option value="м.">м.</option>
								<option value="км.">км.</option>
							</select>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Концентрация пенообразователя">
								<span class="form_legend_text">%</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form_row">
						<div class="form_col_65">
							<div class="form_legend">
								<input type="text" class="form-control" name="" placeholder="Температура замерзания">
								<span class="form_legend_text">C</span>
							</div>
						</div>
						<div class="form_col_35">
							<label class="form-checkbox">
								<input type="checkbox" name="" value="">
								<span class="text-nowrap">Не известно</span>
							</label>
						</div>
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваше имя*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Организация">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="Ваш телефон">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="" placeholder="E-mail*">
					</div>
					<div class="text-center">
						<button type="submit" class="btn-orange btn-send">Отправить</button>
					</div>
				</form>
			</div>
		</div>
		<!-- -->

		<!-- Scripts -->
		<?php include('inc/scripts.inc.php') ?>
		<!-- -->

	</body>
</html>
